
#include <iostream>

using namespace std;

struct Ststystyka
{
  int Porawne;
  int Bledne;
};

void Wyniki(Ststystyka &stat)
{
  cout<<"Liczba dobrych odpowiedzi to:"<<stat.Porawne<<endl;
  cout<<"Liczba blednych odpowiedzi to:"<<stat.Bledne<<endl;
  cout<<"Wynik procentowy poprawnych odpowiedzi:"<<stat.Porawne*100.0/(stat.Porawne+stat.Bledne)<<endl;
}

void Zeruj(Ststystyka &stat)
{
    stat.Bledne=0;
    stat.Porawne=0;
}


