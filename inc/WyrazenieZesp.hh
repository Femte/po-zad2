#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH



#include "LZespolona.hh"


enum Operator { Op_Dodaj, Op_Odejmij, Op_Mnoz, Op_Dziel };



struct WyrazenieZesp
{
  LZespolona   Arg1;
  Operator     Op;
  LZespolona   Arg2;
};


ostream& operator << (std::ostream&, WyrazenieZesp &);
LZespolona Oblicz(WyrazenieZesp );


#endif
