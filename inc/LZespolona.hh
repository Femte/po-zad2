#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH
#include <iostream>


using namespace std;

struct  LZespolona
{
    double   re;
    double   im;
};

LZespolona  operator + (LZespolona,  LZespolona);
LZespolona  operator - (LZespolona,  LZespolona);
LZespolona  operator * (LZespolona,  LZespolona);
LZespolona  operator / (LZespolona,  LZespolona);
LZespolona  operator / (LZespolona, double);
bool operator == (LZespolona, LZespolona);

LZespolona sprzezenie(LZespolona);
double modul2(LZespolona);

void Wyswietl(LZespolona);
void Wczytaj(LZespolona &);

ostream& operator << (std::ostream&, LZespolona &);
istream& operator >> (std::istream&, LZespolona &);

#endif
