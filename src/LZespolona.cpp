#include "LZespolona.hh"
#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;


LZespolona  operator + (LZespolona  Arg1,  LZespolona  Arg2)
{
    LZespolona  Wynik;
    Wynik.re = Arg1.re + Arg2.re;
    Wynik.im = Arg1.im + Arg2.im;
    return Wynik;
}

LZespolona operator - (LZespolona Arg1, LZespolona Arg2)
{
    LZespolona Wynik;
    Wynik.re = Arg1.re - Arg2.re;
    Wynik.im = Arg1.im - Arg2.im;
    return Wynik;
}

LZespolona operator * (LZespolona Arg1, LZespolona Arg2)
{
    LZespolona Wynik;
    Wynik.re = (Arg1.re*Arg2.re)-(Arg1.im*Arg2.im);
    Wynik.im = (Arg1.re*Arg2.im)+(Arg2.re*Arg1.im);
    return Wynik;
}

LZespolona operator / (LZespolona Arg1, LZespolona Arg2)
{
    return Arg1*sprzezenie(Arg2)/modul2(Arg2);
}

LZespolona sprzezenie(LZespolona Arg1)
{
    Arg1.im=Arg1.im*(-1);
    return Arg1;
}

double modul2(LZespolona Arg1)
{
    return  pow(sqrt(pow(Arg1.re,2)+pow(Arg1.im,2)),2);
}

LZespolona operator / (LZespolona Arg1, double liczba)
{
    LZespolona Wynik;
    Wynik.re = Arg1.re/liczba;
    Wynik.im = Arg1.im/liczba;
    return Wynik;
}


bool operator == (LZespolona Arg1, LZespolona Arg2)
{
    if((Arg1.re == Arg2.re) && (Arg1.im == Arg2.im))
    {
        return true;
    }
    return false;
}


ostream& operator << (ostream& wyj, LZespolona &Arg1)
{
    wyj<<'('<<Arg1.re<<showpos<<Arg1.im<<'i'<<')'<<noshowpos;

    return wyj;
}



istream& operator >> (istream& wej, LZespolona &Arg1)
{
    char znak;
    wej>>znak;
    if( znak!='(' )

    {
        wej.setstate (ios::failbit);
        return wej;
    }
    wej>>Arg1.re;
    if( wej.fail() )
    {
        return wej;
    }
    wej>>znak;
    if(znak!='+'&& znak!= '-')
    {
        wej.setstate(ios::failbit);
        return wej;
    }
    wej>>Arg1.im;
    if(wej.fail())
    {
        return wej;
    }
    if(znak=='-')
    {
        Arg1.im*=(-1);
    }
    wej>>znak;
    if(znak!='i')
    {
        wej.setstate(ios::failbit);
        return wej;
    }
    wej>>znak;
    if( znak!=')' )
    {
        wej.setstate(ios::failbit);
        return wej;
    }
return wej;

}
