#include <iostream>
#include <iomanip>
#include "BazaTestu.hh"
#include "Statystyka.hh"

using namespace std;




int main(int argc, char **argv)
{
    Ststystyka staty;
    WyrazenieZesp WyrZ_PytanieTestowe;
    BazaTestu   BazaT = { nullptr, 0, 0 };
    LZespolona Liczba;
    Zeruj(staty);
    if (argc < 2)
    {
        cout << endl;
        cout << " Brak opcji okreslajacej rodzaj testu." << endl;
        cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
        cout << endl;
        return 1;
    }


    if (InicjalizujTest(&BazaT,argv[1]) == false)
    {
        cerr << " Inicjalizacja testu nie powiodla sie." << endl;
        return 1;
    }
    cout << endl;
    cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
    cout << endl;

    while (PobierzNastepnePytanie(&BazaT,&WyrZ_PytanieTestowe))
    {
        cout<< WyrZ_PytanieTestowe<<" = ";
        for(int i =0; i<4; i++)
        {
            cin>>Liczba;
            if(!cin.fail())
            {
                break;
            }
            if(i<3)
            {
                         cin.clear();
            cin.ignore(1000,'\n');
            cout<<"Blad skladni sprobuj jeszcze raz"<<endl;
            }

        }

        if(cin.fail())
        {
            cin.clear();
            cin.ignore(1000,'\n');
            cout<<"blad skladni"<<endl;
            staty.Bledne=staty.Bledne+1;
            cout<<staty.Bledne;
        }
        else if(Liczba == Oblicz(WyrZ_PytanieTestowe))
        {
            cout<<"Poprawna odpowiedz"<<endl;
            staty.Porawne+=1;

        }
        else
        {
            Liczba = Oblicz(WyrZ_PytanieTestowe);
            cout<<"Bledna odpowiedz. Poprawna odpowiedz to:"<< Liczba <<endl;
            staty.Bledne+=1;

        }

        cout << endl;
    }

    cout << endl;
    cout << "Koniec testu" << endl;
    cout << "Twoja statystyka" << endl;
    Wyniki(staty);


    cout << endl;
}


/* porownanie dwoch liczb zespolonych*/
